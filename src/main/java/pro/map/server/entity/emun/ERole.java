package pro.map.server.entity.emun;

public enum ERole {
	ADMIN_ROL,
	TECNICO_ROL,
	SUPERVISOR_ROL,
	CLIENTE_ROL
}
