package pro.map.server.entity;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
@Entity @Builder
@Table(name="solicitud")
public class SolicitudEntity {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="id_tecnico")
	private Long id_tecnico;
	
	@NotBlank
	@Size(max = 80)
	@Column(name="titulo")
	private String titulo;
	
	@NotBlank
	@Size(max = 120)
	@Column(name="descripcion")
	private String descripcion;
	
	@NotBlank
	@Size(max = 180)
	@Column(name="descripcion_tecnica")
	private String descripcion_tecnica;
	
    @CreationTimestamp
	@Column(name="fecha")
	private Date fecha;
	
    @Column(name="asignado")
    private boolean asignado;
    
    @Column(name="finalizado")
    private boolean finalizado;
   
	@ManyToMany(fetch= FetchType.LAZY, targetEntity = UsuarioEntity.class, cascade = CascadeType.MERGE)
	@JoinTable(name = "users_solicitud", joinColumns = @JoinColumn(name = "solicitud_id"), inverseJoinColumns = @JoinColumn(name = "user_id"))
	private List<UsuarioEntity> usuario;
	
	public void save() {
	    createdAt();
	}

	@PrePersist
	void createdAt() {
	    this.fecha = new Date();
	    this.finalizado = false;
	    this.asignado = false;
	}

	
}
