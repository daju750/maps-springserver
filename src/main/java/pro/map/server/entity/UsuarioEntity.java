package pro.map.server.entity;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
@Entity @Builder
@Table(name="usuario")
public class UsuarioEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Email
	@NotBlank
	@Size(max = 80)
	@Column(name="email",unique=true)
	@JsonProperty(value = "email")
	private String email;
	
	@NotBlank
	@Size(max = 80)
	@Column(name="username",unique=true)
	@JsonProperty(value = "username")
	private String username;
	
	@NotBlank
	@Column(name="password")
	@JsonProperty(value = "password")
	private String password;
	
	@Column(name="direccion")
	@JsonProperty(value = "direccion")
	private String direccion;
	
	@Column(name="lactitud")
	@JsonProperty(value = "lacitud")
	private String lactitud;
	
	@Column(name="longitud")
	@JsonProperty(value = "longitud")
	private String longitud;
	
	@JsonProperty(value = "roles")
	@ManyToMany(fetch= FetchType.EAGER, targetEntity = RolEntity.class, cascade = CascadeType.MERGE)
	@JoinTable(name = "users_roles", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "rol_id"))
	private List<RolEntity> roles;

}
