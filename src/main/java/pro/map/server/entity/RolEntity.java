package pro.map.server.entity;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pro.map.server.entity.emun.ERole;

@Data @AllArgsConstructor @NoArgsConstructor
@Entity @Builder
@Table(name="rol")
public class RolEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="id")
	@JsonProperty(value = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="nombre", length = 25)
	@JsonProperty(value = "nombre")
	@NotBlank
	@Enumerated(EnumType.STRING)
	private ERole nombre;

	/*
	@ManyToOne(targetEntity = RolEntity.class, cascade = CascadeType.PERSIST)
	@JoinTable(name = "users_roles", joinColumns = @JoinColumn(name = "rol_id"), inverseJoinColumns = @JoinColumn(name = "user_id"))
	private Set<UsuarioEntity> users;
	*/
	
	
}
