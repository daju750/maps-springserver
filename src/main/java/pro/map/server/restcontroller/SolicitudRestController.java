package pro.map.server.restcontroller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pro.map.server.dto.ActualizarSolicitudRequestDTO;
import pro.map.server.dto.SolicitudRequestDTO;
import pro.map.server.entity.SolicitudEntity;
import pro.map.server.entity.UsuarioEntity;
import pro.map.server.servcie.impl.SolicitudServiceImpl;
import pro.map.server.servcie.impl.UsuarioServiceImpl;

@RestController
@RequestMapping("/solicitud")
public class SolicitudRestController {
	
	@Autowired
	private UsuarioServiceImpl usuarioimpl;

	@Autowired
	private SolicitudServiceImpl solicitudimpl;
	
	@PostMapping
	@ResponseBody
	public  ResponseEntity<?> PostInsertar(@Valid @RequestBody SolicitudRequestDTO solicituddto){
		SolicitudEntity solicitud = new SolicitudEntity();
		List<UsuarioEntity> user = new ArrayList<UsuarioEntity>();
		UsuarioEntity usuario = new UsuarioEntity();
		
		try {
		usuario.setId(this.usuarioimpl.ConsultarUsuarioId(solicituddto.getUsername()));
		usuario.setEmail("");
		usuario.setPassword("");
		usuario.setUsername(solicituddto.getUsername());
		user.add(0,usuario);
		
		solicitud.setTitulo(solicituddto.getTitulo());
		solicitud.setDescripcion(solicituddto.getDescripcion());
		solicitud.setUsuario(user);
		solicitud.save();
		
		this.solicitudimpl.guardar(solicitud);
		}catch(Exception ex) { System.out.println(ex); }
		
		
		return ResponseEntity.ok(solicitud);
	}
	
	@GetMapping
	@ResponseBody
	public  ResponseEntity<?> ListarUsuariosNoAsignados(){
		
		return ResponseEntity.ok(this.solicitudimpl.ListarClientesNoAsignados());
	}
	
	@GetMapping("/detalle/{id}")
	@ResponseBody
	public  ResponseEntity<?> DetalleUsuarioNoAsignado(@Valid @PathVariable("id") String id){
		return ResponseEntity.ok(this.solicitudimpl.DetalleClienteNoAsignado(id));
	}
	
	@PostMapping("/actulizar")
	@ResponseBody
	public  ResponseEntity<?> ActulizarSolcictud(@Valid @RequestBody ActualizarSolicitudRequestDTO actualizardto){
		this.solicitudimpl.AsignarSolicitudTecnico(actualizardto);
		return ResponseEntity.ok("Asignacion Completa");
	}
	
	@GetMapping("/asginadas/lista")
	@ResponseBody
	public  ResponseEntity<?> ListarSolicitudesAsginadas(){
		return ResponseEntity.ok(this.solicitudimpl.SolicitudesAsginadas());
	}
	
}
