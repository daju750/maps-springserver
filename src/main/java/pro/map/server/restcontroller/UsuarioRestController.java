package pro.map.server.restcontroller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pro.map.server.dto.MapsDireccion;
import pro.map.server.entity.UsuarioEntity;
import pro.map.server.servcie.impl.UsuarioServiceImpl;

@RestController
@RequestMapping("/usuario")
public class UsuarioRestController {

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UsuarioServiceImpl usuarioservicioimpl;
	
	@PostMapping
	@ResponseBody
	public  ResponseEntity<?> PostInsertar(@Valid @RequestBody UsuarioEntity usuario){
		/*
		Set<RolEntity> roles = usuario.getRoles().stream()
				.map(role-> RolEntity.builder()
						.nombre(ERole.valueOf("TECNICO_ROL"))
						.build())
				.collect(Collectors.toSet());
		
		UsuarioEntity user = UsuarioEntity.builder()
				.username(usuario.getUsername())
				.password(usuario.getPassword())
				.email(usuario.getEmail())
				.roles(roles)
				.build();
		*/
		usuario.setPassword(this.passwordEncoder.encode(usuario.getPassword()));
		return ResponseEntity.ok(this.usuarioservicioimpl.guardar(usuario));
	}
	
	@GetMapping("/listar")
	@ResponseBody
	public  List<UsuarioEntity>  GetListarTodos(){
		return this.usuarioservicioimpl.ListarTodos();
	}
	
	@GetMapping("/maps/{id}")
	@ResponseBody
	public MapsDireccion CordenadasUsuario(@Valid @PathVariable("id") String id){
		return this.usuarioservicioimpl.CordenadasCliente(id);
	}
	
	
	
	
}
