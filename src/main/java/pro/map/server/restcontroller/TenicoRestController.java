package pro.map.server.restcontroller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pro.map.server.dto.ListaSolicitudesAsignadasTecnico;
import pro.map.server.dto.ListaTecnicosRequestDTO;
import pro.map.server.dto.RegistroFinalizadoTecnico;
import pro.map.server.servcie.impl.TecnicoServiceImpl;
import pro.map.server.servcie.impl.UsuarioServiceImpl;

@RestController
@RequestMapping("/tecnico")
public class TenicoRestController {

	@Autowired
	UsuarioServiceImpl usuarioimpl;
	
	@Autowired
	TecnicoServiceImpl tecnicoimpl;
	
	@GetMapping("/lista")
	@ResponseBody
	public  List<ListaTecnicosRequestDTO>  GetListarTodos(){
		
		List<ListaTecnicosRequestDTO> lista = this.usuarioimpl.ListarTecnicos();
		
		return lista;
	}
	
	@GetMapping("/solicitudes/lista/{username}")
	@ResponseBody
	public  List<ListaSolicitudesAsignadasTecnico>  ListaSolcitudesAsginadasLista(@Valid @PathVariable("username") String username){
		
		return this.tecnicoimpl.ListaSolicitudesAsginadas(username);
	}
	
	@GetMapping("/solicitudes/finalizadas/lista/{username}")
	@ResponseBody
	public  List<ListaSolicitudesAsignadasTecnico>  ListaSolcitudesFinalizadosLista(@Valid @PathVariable("username") String username){
		System.out.println(username);
		return this.tecnicoimpl.ListaSolicitudesFinalizadas(username);
	}
	
	@PostMapping("/solicitud/finalizada")
	@ResponseBody
	public ResponseEntity<?>  SolicitudTecnicaFinalizada(@Valid @RequestBody RegistroFinalizadoTecnico registro){
		
		this.tecnicoimpl.RegistroFinalizado(registro.getId(),registro.getDescripcion_tecnica());
		return ResponseEntity.ok("Registro Finalizado");
	}
	
	
}
