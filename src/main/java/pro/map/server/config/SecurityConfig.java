package pro.map.server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import static org.springframework.security.config.Customizer.withDefaults;

import pro.map.server.jwt.JwtAthenticationFilter;
import pro.map.server.jwt.JwtAthorizationFilter;
import pro.map.server.jwt.JwtUtils;
import pro.map.server.servcie.impl.UserDetailsServiceImpl;


import org.springframework.beans.factory.annotation.Autowired;

@Configuration
public class SecurityConfig{

	@Autowired
	JwtUtils jwtUtils;
	
	@Autowired
	UserDetailsServiceImpl userDetailsImpl;
	
	@Autowired
	JwtAthorizationFilter jwtAthorizationFilter;
	
	@Bean
	protected SecurityFilterChain filterChain(HttpSecurity http, AuthenticationManager authenticationManager) throws Exception {
		
		JwtAthenticationFilter jwtAthenticationFilter = new JwtAthenticationFilter(jwtUtils);
		jwtAthenticationFilter.setAuthenticationManager(authenticationManager);
		jwtAthenticationFilter.setFilterProcessesUrl("/login");
		
		http
			.csrf(config -> config.disable())
			.cors(withDefaults())
			.authorizeHttpRequests(authorize -> {
				authorize.requestMatchers(new AntPathRequestMatcher("/login")).permitAll();
				authorize.requestMatchers(new AntPathRequestMatcher("/jwt")).permitAll();
				//authorize.requestMatchers(new AntPathRequestMatcher("/solicitud")).permitAll();
				authorize.anyRequest().authenticated();
				})
			.sessionManagement(session -> {
                session.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
            })
			//authorize.requestMatchers(new AntPathRequestMatcher("/rol/listar")).permitAll().anyRequest().authenticated())
			.addFilter(jwtAthenticationFilter)
			.addFilterBefore(jwtAthorizationFilter,UsernamePasswordAuthenticationFilter.class);
		return http.build();
	}
	
	
	@Bean
	protected WebMvcConfigurer corsConfigurer(){
	   String[] allowDomains = new String[2];
	   allowDomains[0] = "http://localhost:4200";

	   //System.out.println("CORS configuration....");
	   return new WebMvcConfigurer() {
	      @Override
	      public void addCorsMappings(CorsRegistry registry) {
	         registry.addMapping("/**").allowedOrigins(allowDomains);
	      }
	   };
	}
	
	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@SuppressWarnings("removal")
	@Bean 
	AuthenticationManager authenticationManager(HttpSecurity httpSecurity, PasswordEncoder passwordEncoder) throws Exception{
		return httpSecurity.getSharedObject(AuthenticationManagerBuilder.class).userDetailsService(userDetailsImpl).passwordEncoder(passwordEncoder).and().build();
	}

	
}
