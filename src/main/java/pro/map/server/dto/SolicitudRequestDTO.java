package pro.map.server.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class SolicitudRequestDTO {

	@JsonProperty(value = "titulo")
	private String titulo;
	
	@JsonProperty(value = "descripcion")
	private String descripcion;
	
	@JsonProperty(value = "fecha")
	private Date fecha;

	@JsonProperty(value = "username")
	private String username;
}
