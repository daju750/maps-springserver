package pro.map.server.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class UsuarioRequestDTO {

	@Email
	@NotBlank
	@JsonProperty(value = "email")
	private String email;
	@NotBlank
	@JsonProperty(value = "username")
	private String username;
	@NotBlank
	@JsonProperty(value = "password")
	private String password;
	@JsonProperty(value = "rol")
	private String rol;
}
