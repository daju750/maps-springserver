package pro.map.server.dto.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class UsuariosNoAsignadosModelDTO {

	Long id;
	String username;
	String titulo;
	String descripcion;
	Date fecha;
	String direccion;
}
