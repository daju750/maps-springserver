package pro.map.server.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class ListaSolicitudesAsignadasTecnico {

	private String id;
	private String titulo;
	private String fecha;
	
}
