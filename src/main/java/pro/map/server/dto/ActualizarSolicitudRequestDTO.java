package pro.map.server.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class ActualizarSolicitudRequestDTO {

	String id;
	String id_tecnico;
	
}
