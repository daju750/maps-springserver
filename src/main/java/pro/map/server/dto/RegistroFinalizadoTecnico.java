package pro.map.server.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class RegistroFinalizadoTecnico {

	@JsonProperty(value = "id")
	String id;
	@JsonProperty(value = "descripcion_tecnica")
	String descripcion_tecnica;
	
}
