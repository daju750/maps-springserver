package pro.map.server.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pro.map.server.entity.UsuarioEntity;

public interface UsuarioRepositorio extends JpaRepository<UsuarioEntity,Long> {

	Optional<UsuarioEntity> findByUsername(String username);
	
	@Query(value="select u.id from usuario u where u.username=?1",nativeQuery = true)
	Long id(String username);
	
}
