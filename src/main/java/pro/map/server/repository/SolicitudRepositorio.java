package pro.map.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pro.map.server.entity.SolicitudEntity;

public interface SolicitudRepositorio extends JpaRepository<SolicitudEntity,Long>{

}
