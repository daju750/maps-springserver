package pro.map.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pro.map.server.entity.RolEntity;

public interface RolRepositorio extends JpaRepository<RolEntity,Long>{
	
	@Query(value = "select id,nombre from rol u where u.id=?1",nativeQuery = true)
	RolEntity JQLPBuscarPorId(Long id);
	
}
