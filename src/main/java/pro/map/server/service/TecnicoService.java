package pro.map.server.service;

import java.util.List;

import pro.map.server.dto.ListaSolicitudesAsignadasTecnico;

public interface TecnicoService {

	public List<ListaSolicitudesAsignadasTecnico> ListaSolicitudesAsginadas(String username);
	public List<ListaSolicitudesAsignadasTecnico> ListaSolicitudesFinalizadas(String username);
	public void RegistroFinalizado(String id, String descripcion_tecnica);
	
}
