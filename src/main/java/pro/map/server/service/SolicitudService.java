package pro.map.server.service;

import java.util.List;

import pro.map.server.dto.ActualizarSolicitudRequestDTO;
import pro.map.server.dto.model.UsuariosNoAsignadosModelDTO;
import pro.map.server.entity.SolicitudEntity;

public interface SolicitudService {

	public SolicitudEntity guardar(SolicitudEntity solicitud);
	public List<UsuariosNoAsignadosModelDTO> ListarClientesNoAsignados();
	public UsuariosNoAsignadosModelDTO DetalleClienteNoAsignado(String id);
	public void AsignarSolicitudTecnico(ActualizarSolicitudRequestDTO actualizardto);
	public List<UsuariosNoAsignadosModelDTO> SolicitudesAsginadas();
}
