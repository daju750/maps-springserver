package pro.map.server.service;

import java.util.List;

import pro.map.server.entity.RolEntity;

public interface RolService {

	public RolEntity guardar(RolEntity rol);
	public List<RolEntity> listar();
	public RolEntity BuscarRolId(Long id);
	public String RolUsuario(String username);
	
}
