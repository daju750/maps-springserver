package pro.map.server.service;

import java.util.List;

import pro.map.server.dto.ListaTecnicosRequestDTO;
import pro.map.server.dto.MapsDireccion;
import pro.map.server.entity.UsuarioEntity;

public interface UsuarioService {

	public UsuarioEntity guardar(UsuarioEntity usuario);
	public List<UsuarioEntity> ListarTodos();
	public Long ConsultarUsuarioId(String username);
	public List<ListaTecnicosRequestDTO> ListarTecnicos();
	public MapsDireccion CordenadasCliente(String id);
}
