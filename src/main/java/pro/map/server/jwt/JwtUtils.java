package pro.map.server.jwt;

import java.io.Serializable;
import java.security.Key;
import java.util.Date;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import pro.map.server.servcie.impl.RolServiceImpl;

@Component
public class JwtUtils implements  Serializable {

	private static final long serialVersionUID = 1L;

	public static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;
	
	@Autowired
	private RolServiceImpl rolServiceImpl;
	
	private static String secretKey="F9D27A38917BE4FE6E45D6FADFDS2E89466A456D456SD4F6465ASD456FDSAB5C6D6E";
	private Long timeExpiration=(long) 86400000;
	
	public String generarAccesToken(String username) {
		return Jwts.builder() 
				.setSubject(username)
				.claim("rol",this.rolServiceImpl.RolUsuario(username))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis()+this.timeExpiration))
				//.signWith(secretKey,SignatureAlgorithm.HS512).compact();
				.signWith(getSignatureKey(),SignatureAlgorithm.HS256).compact();
	}
	
	//Encrypta en base 64
	public Key getSignatureKey() {
		byte[] keyBytes = Decoders.BASE64.decode(secretKey);
		//java.util.Base64.getUrlDecoder().decode(this.secretKey);
		return Keys.hmacShaKeyFor(keyBytes);
	}
	
	//Valiar si el token es valido
	public boolean isTokenValid(String token) {
		try{
			Jwts.parserBuilder().setSigningKey(getSignatureKey()).build().parseClaimsJws(token).getBody();
			return true;
		}catch(Exception ex) {
			return false;
		}	
	}
	
	//obtener el username de token
	public String getUsernameFromToken(String token) {
		return getClaim(token,Claims::getSubject);
	}
	
	//Obtener solo un Claim
	public <T> T getClaim(String token,Function<Claims,T> claimsFunction) {
		Claims claims = extractAllClaims(token);
		return claimsFunction.apply(claims);
	}
	
	// Extraer todos los claims
	public Claims extractAllClaims(String token) {
		return Jwts.parserBuilder().setSigningKey(getSignatureKey()).build().parseClaimsJws(token).getBody();
	}
	
}
