package pro.map.server.jwt;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import pro.map.server.servcie.impl.UserDetailsServiceImpl;

@Component
public class JwtAthorizationFilter extends OncePerRequestFilter{

	@Autowired
	private JwtUtils jwtUtils;
	
	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;
	
	@Override
	protected void doFilterInternal(@NonNull HttpServletRequest request,@NonNull  HttpServletResponse response,@NonNull  FilterChain filterChain)
			throws ServletException, IOException {
		
			String tokenHearder = request.getHeader("Authorization");
			
			if(tokenHearder!=null && tokenHearder.startsWith("Bearer ")) {
				String token = tokenHearder.subSequence(7,tokenHearder.length()).toString();
				
				if(this.jwtUtils.isTokenValid(token)){
					String username = this.jwtUtils.getUsernameFromToken(token);
					UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername(username);
					
					UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(username,null,userDetails.getAuthorities());
					
					SecurityContextHolder.getContext().setAuthentication(authToken);
				}
			}
			filterChain.doFilter(request, response);
	}

}
