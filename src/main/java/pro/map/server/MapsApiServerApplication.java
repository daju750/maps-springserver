package pro.map.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MapsApiServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MapsApiServerApplication.class, args);
	}

}
