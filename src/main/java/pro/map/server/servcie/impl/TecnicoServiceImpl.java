package pro.map.server.servcie.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.map.server.dto.ListaSolicitudesAsignadasTecnico;
import pro.map.server.repository.UsuarioRepositorio;
import pro.map.server.service.TecnicoService;

@Service
public class TecnicoServiceImpl implements TecnicoService{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private UsuarioRepositorio usuariorepo;
	
	public List<ListaSolicitudesAsignadasTecnico> ListaSolicitudesAsginadas(String username) {
		
		Long id = this.usuariorepo.id(username);
		@SuppressWarnings("deprecation")
		List<ListaSolicitudesAsignadasTecnico> lista = jdbcTemplate.query("select id,titulo,fecha from solicitud where id_tecnico = ? and asignado=true and finalizado=false ORDER by fecha ASC",
				new Object[]{id},
				(rs, rowNum) -> new ListaSolicitudesAsignadasTecnico(
				rs.getString("id"),
				rs.getString("titulo"),
				rs.getString("fecha")
				));
		return lista;
	}

	public List<ListaSolicitudesAsignadasTecnico> ListaSolicitudesFinalizadas(String username) {
		Long id = this.usuariorepo.id(username);
		System.out.println("ID: "+id+", Username: "+username);
		@SuppressWarnings("deprecation")
		List<ListaSolicitudesAsignadasTecnico> lista = jdbcTemplate.query("select id,titulo,fecha from solicitud where id_tecnico = ? and finalizado=true ORDER by fecha ASC",
				new Object[]{id},
				(rs, rowNum) -> new ListaSolicitudesAsignadasTecnico(
				rs.getString("id"),
				rs.getString("titulo"),
				rs.getString("fecha")
				));
		return lista;
	}
	
	public void RegistroFinalizado(String id, String descripcion_tecnica) {
		this.jdbcTemplate.update("update solicitud set finalizado = true, descripcion_tecnica='"+descripcion_tecnica+"' where id = "+id);
	}




}
