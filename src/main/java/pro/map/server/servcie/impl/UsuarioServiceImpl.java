package pro.map.server.servcie.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.map.server.dto.ListaTecnicosRequestDTO;
import pro.map.server.dto.MapsDireccion;
import pro.map.server.entity.UsuarioEntity;
import pro.map.server.repository.UsuarioRepositorio;
import pro.map.server.service.UsuarioService;

@Service
public class UsuarioServiceImpl  implements UsuarioService {

	@Autowired
	private UsuarioRepositorio usuariorepo;
	
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	public UsuarioEntity guardar(UsuarioEntity usuario) {
		return this.usuariorepo.save(usuario);
	}

	public List<UsuarioEntity> ListarTodos() {
		return this.usuariorepo.findAll();
	}

	public Long ConsultarUsuarioId(String username) {
		return this.usuariorepo.id(username);
	}

	public List<ListaTecnicosRequestDTO> ListarTecnicos() {
		@SuppressWarnings("deprecation")
		List<ListaTecnicosRequestDTO> listaTecnicos = this.jdbcTemplate.query("select id,username from usuario where id in(select user_id from users_roles where rol_id = (select id from rol where nombre='TECNICO_ROL'))",
				new Object[]{},
				(rs, rowNum) -> new ListaTecnicosRequestDTO (
				rs.getLong("id"),
				rs.getString("username")
				));
		
		return  listaTecnicos;
	}

	@SuppressWarnings("deprecation")
	public MapsDireccion CordenadasCliente(String id) {
		return this.jdbcTemplate.queryForObject("select lactitud,longitud from usuario where id=(select user_id from users_solicitud where solicitud_id = ?)",
				new Object[]{id},
				(rs, rowNum) -> new MapsDireccion (
				rs.getString("lactitud"),
				rs.getString("longitud")
				));
	}
	
	
	
}
