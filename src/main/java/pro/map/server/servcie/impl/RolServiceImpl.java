package pro.map.server.servcie.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.map.server.entity.RolEntity;
import pro.map.server.repository.RolRepositorio;
import pro.map.server.service.RolService;

@Service
public class RolServiceImpl implements RolService{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private RolRepositorio rolrepo;
	
	public RolEntity guardar(RolEntity rol) {
		return this.rolrepo.save(rol);
	}

	public List<RolEntity> listar() {
		return this.rolrepo.findAll();
	}

	public RolEntity BuscarRolId(Long id) {
		return this.rolrepo.JQLPBuscarPorId(id);
	}

	public String RolUsuario(String username) {
		return jdbcTemplate.queryForObject("select nombre from rol where id=(select rol_id from users_roles where user_id=(select id from usuario where username='"+username+"'))",String.class);
	}

}
