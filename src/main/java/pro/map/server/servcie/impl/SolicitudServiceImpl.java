package pro.map.server.servcie.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.map.server.dto.ActualizarSolicitudRequestDTO;
import pro.map.server.dto.model.UsuariosNoAsignadosModelDTO;
import pro.map.server.entity.SolicitudEntity;
import pro.map.server.repository.SolicitudRepositorio;
import pro.map.server.service.SolicitudService;


@Service
public class SolicitudServiceImpl implements SolicitudService{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private SolicitudRepositorio solicitudRepo;
	
	public SolicitudEntity guardar(SolicitudEntity solicitud) {
		
		return this.solicitudRepo.save(solicitud);
	}

	@SuppressWarnings("deprecation")
	public List<UsuariosNoAsignadosModelDTO> ListarClientesNoAsignados() {
		
		List<UsuariosNoAsignadosModelDTO> listaUsuarios = null;
		
		try {
			
		listaUsuarios = jdbcTemplate.query("select s.id as id_solicitud,u.username,s.titulo,s.descripcion,s.fecha,u.direccion from solicitud s,users_solicitud us,usuario u where us.solicitud_id=s.id and u.id=us.user_id and s.asignado=false and s.finalizado=false order by s.fecha ASC",
					new Object[]{},
					(rs, rowNum) -> new UsuariosNoAsignadosModelDTO(
					rs.getLong("id_solicitud"),
					rs.getString("username"),
					rs.getString("titulo"),
					rs.getString("descripcion"),
					rs.getDate("fecha"),
					rs.getString("direccion")
					));
		}catch(Exception ex){listaUsuarios = new ArrayList<>(); System.out.println("Invalido");	}
		
		return listaUsuarios;
	}

	@SuppressWarnings("deprecation")
	public UsuariosNoAsignadosModelDTO DetalleClienteNoAsignado(String id) {

		String sql = "select s.id as id_solicitud,u.username,s.titulo,s.descripcion,s.fecha,u.direccion from solicitud s,users_solicitud us,usuario u where us.solicitud_id=s.id and u.id=us.user_id and s.id = ?";
		try{
		UsuariosNoAsignadosModelDTO detalleSolicitud = jdbcTemplate.queryForObject(sql,
					new Object[]{id},
					(rs,row) -> new UsuariosNoAsignadosModelDTO(
					rs.getLong("id_solicitud"),
					rs.getString("username"),
					rs.getString("titulo"),
					rs.getString("descripcion"),
					rs.getDate("fecha"),
					rs.getString("direccion")
					));
			
		return detalleSolicitud;
		
		}catch(Exception ex) { return new UsuariosNoAsignadosModelDTO(); }
		
	}


	public void AsignarSolicitudTecnico(ActualizarSolicitudRequestDTO actualizardto) {
		this.jdbcTemplate.update("update solicitud set asignado = true, id_tecnico = "+actualizardto.getId_tecnico()+" where id = "+actualizardto.getId());
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<UsuariosNoAsignadosModelDTO> SolicitudesAsginadas() {
	List<UsuariosNoAsignadosModelDTO> listaUsuarios = null;

		try {
			
		listaUsuarios = jdbcTemplate.query("select s.id as id_solicitud,u.username,s.titulo,s.descripcion,s.fecha,u.direccion from solicitud s,users_solicitud us,usuario u where us.solicitud_id=s.id and u.id=us.user_id and s.asignado=true order by s.fecha ASC",
					new Object[]{},
					(rs, rowNum) -> new UsuariosNoAsignadosModelDTO(
					rs.getLong("id_solicitud"),
					rs.getString("username"),
					rs.getString("titulo"),
					rs.getString("descripcion"),
					rs.getDate("fecha"),
					rs.getString("direccion")
					));
		System.out.println("Valido");	
		}catch(Exception ex){listaUsuarios = new ArrayList<>(); System.out.println("Invalido");	}
		
		return listaUsuarios;
		
	}
	
	

}
