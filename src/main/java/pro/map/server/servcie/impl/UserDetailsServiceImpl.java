package pro.map.server.servcie.impl;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import pro.map.server.entity.UsuarioEntity;
import pro.map.server.repository.UsuarioRepositorio;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UsuarioRepositorio usuariorepo;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		UsuarioEntity user = this.usuariorepo.findByUsername(username).orElseThrow(()-> new UsernameNotFoundException("El usuario "+username+" no es valido"));
		Collection<? extends GrantedAuthority> authorities = user.getRoles().stream().map(
				role->new SimpleGrantedAuthority("ROLE_"+role.getNombre().name())).collect(Collectors.toList()
				);
		return new User(user.getUsername(),user.getPassword(),true,true,true,true,authorities);
	}
	

}
